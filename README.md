# terraform-proxmox

Terraform repository for Proxmox provider.

## Prerequisite

### Template creation

In order to create a linux template, run the *template_creation.sh* script under _script_ or use following command (creation of an Ubuntu template using the Ubuntu 22.04 cloud image with the id 10001):

```
apt install libguestfs-tools
wget https://gitlab.com/homelab3/terraform/terraform-proxmox/-/raw/main/scripts/create_template.sh \
bash template_creation.sh -u https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img -n ubuntu-22.04-template -i 10001
```

### API key

Create a terraform user and an API key:
```
pveum role add ROLE_NAME -privs "Datastore.AllocateSpace Datastore.Audit Pool.Allocate Sys.Audit Sys.Console Sys.Modify VM.Allocate VM.Audit VM.Clone VM.Config.CDROM VM.Config.Cloudinit VM.Config.CPU VM.Config.Disk VM.Config.HWType VM.Config.Memory VM.Config.Network VM.Config.Options VM.Migrate VM.Monitor VM.PowerMgmt SDN.Use"
pveum user add terraform-prov@pve --password PASSWORD
pveum aclmod / -user terraform-prov@pve -role ROLE_NAME
pveum user token add terraform-prov@pve terraform-token --privsep=0
```

Set global variables for Proxmox connection:

```
export PM_API_TOKEN_ID=""
export PM_API_TOKEN_SECRET=""
```
