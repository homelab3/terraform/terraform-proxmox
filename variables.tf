variable "description" {
  type        = string
  description = "Description of the virtual machine"
  default     = ""
}

variable "tags" {
  type        = string
  description = "Tag(s) to put on the VM"
  default     = ""
}

variable "target_node" {
  type        = string
  description = "Target node for the virtual machine"
}

variable "template_id" {
  type        = number
  description = "Template id to use for the creation of the VM"
  default     = 10001
}

variable "vm_id" {
  type        = number
  description = "ID of the virtual machine"
  default     = 0
}
variable "name" {
  type        = string
  description = "The name of the VM"
  default     = ""
}

variable "cpu_nb" {
  type        = number
  description = "The number of CPU"
  default     = 2
}

variable "ram" {
  type        = number
  description = "SThe sze of the RAM in MB"
  default     = 1024
}

variable "pool" {
  type        = string
  description = "The pool to use"
  default     = ""
}

variable "ip_v4" {
  type        = string
  description = "The IPv4 of the VM"
  default     = ""
}

variable "datastore" {
  type        = string
  description = "The datastore for the disks"
}

variable "sda_size" {
  type        = string
  description = "The size of first disk in GB"
  default     = "16G"
}

variable "disks" {
  description = "List of the secondary disks"
  type = map(object({
    slot = number
    size = string
  }))
}

variable "bridge_name" {
  type        = string
  description = "Name of the network bridge"
  default     = "vmbr0"
}

variable "gateway" {
  type        = string
  description = "IP of the gateway"
  default     = "192.168.1.1"
}

variable "nameserver" {
  type        = list(string)
  description = "List of DNS servers IP, separated by comma"
  default     = ["192.168.1.2", "1.1.1.1"]
}

variable "searchdomain" {
  type        = string
  description = "Search domain"
  default     = "local.myops.ch"
}

variable "environment" {
  type = string
  description = "Environment"
  default = "test"
}
