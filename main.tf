#################################################################
#                   VIRTUAL MACHINE                             #
#################################################################

resource "proxmox_virtual_environment_vm" "vm" {
  name        = var.name
  description = var.description
  node_name   = var.target_node 
  clone {
    vm_id = var.template_id
  }
  vm_id       = var.vm_id
  cpu {
    cores       = var.cpu_nb
    sockets     = "1"
    type        = "host"
  }
  memory {
    dedicated = var.ram
  }
  pool_id     = var.pool
  agent {
    enabled   = true 
  }

  # Disks creation
  disk {
    size     = var.sda_size
    interface     = "scsi0"
    datastore_id  = var.datastore
    iothread = true
  }

  dynamic "disk" {
    for_each = var.disks
    content {
      size     = disk.value["size"]
      interface     = "scsi${disk.value["slot"]}"
      datastore_id  = var.datastore
      iothread = true
    }
  }

  # Network configuraiton
  network_device {
    model  = "virtio"
    bridge = var.bridge_name
  }

  initialization {
    ip_config {
      ipv4 {
        address = "${var.ip_v4}/24"
        gateway = var.gateway
      }
    }
    dns {
      domain = var.searchdomain
      servers = var.nameserver
    }
  }
}

