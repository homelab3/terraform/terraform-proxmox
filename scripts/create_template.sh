#!/bin/bash

#Create template
#args:
# vm_id
# vm_name
# file name in the current directory
function create_template() {
    #Print all of the configuration
    echo "Creating template $2 ($1)"
    # Customize VM
    virt-customize -a $3 --install qemu-guest-agent
    #Create new VM
    #Feel free to change any of these to your liking
    qm create $1 --name $2 --ostype l26
    #Set networking to default bridge
    qm set $1 --net0 virtio,bridge=vmbr0
    #Set display to serial
    qm set $1 --serial0 socket --vga qxl
    #Set memory, cpu, type defaults
    #If you are in a cluster, you might need to change cpu type
    qm set $1 --memory 2048 --cores 2 --cpu host
    #Set boot device to new file
    qm set $1 --scsi0 ${storage}:0,import-from="$(pwd)/$3",discard=on
    #Set scsi hardware as default boot disk using virtio scsi single
    qm set $1 --boot order=scsi0 --scsihw virtio-scsi-single
    #Enable Qemu guest agent in case the guest has it available
    qm set $1 --agent enabled=1,fstrim_cloned_disks=1
    #Add cloud-init device
    qm set $1 --ide2 ${storage}:cloudinit
    #Set CI ip config
    #IP6 = auto means SLAAC (a reliable default with no bad effects on non-IPv6 networks)
    #IP = DHCP means what it says, so leave that out entirely on non-IPv4 networks to avoid DHCP delays
    qm set $1 --ipconfig0 "ip6=auto,ip=dhcp"
    #Import the ssh keyfile
    qm set $1 --sshkeys ${ssh_keyfile}
    #If you want to do password-based auth instaed
    #Then use this option and comment out the line above
    qm set $1 --cipassword ${password}
    #Add the user
    qm set $1 --ciuser ${username}
    #Resize the disk to 8G, a reasonable minimum. You can expand it more later.
    #If the disk is already bigger than 8G, this will fail, and that is okay.
    qm disk resize $1 scsi0 8G
    #Make it a template
    qm template $1

    #Remove file when done
    rm $3
}


#Path to your ssh authorized_keys file
#Alternatively, use /etc/pve/priv/authorized_keys if you are already authorized
#on the Proxmox system
#export ssh_keyfile=/root/.ssh/id_rsa.pub
export ssh_keyfile=/etc/pve/priv/authorized_keys
#Username to create on VM template
export username=administrator
export password=password
#Name of your storage
export storage=pmoxpool1a

#The images that I've found premade
#Feel free to add your own

## Debian
#Bookworm (12) (stable)
#wget "https://cloud.debian.org/images/cloud/bookworm/latest/debian-12-genericcloud-amd64.qcow2"
#qm destroy 10001
#create_template 10001 "temp-debian-12" "debian-12-genericcloud-amd64.qcow2"

## Ubuntu
#24.04 (Noble Numbat) LTS
wget "https://cloud-images.ubuntu.com/releases/24.04/release/ubuntu-24.04-server-cloudimg-amd64.img"
wget -O puppet.deb https://apt.puppet.com/puppet8-release-jammy.deb
virt-copy-in -a ubuntu-24.04-server-cloudimg-amd64.img puppet.deb /tmp
virt-customize -a ubuntu-24.04-server-cloudimg-amd64.img --run-command 'dpkg -i /tmp/puppet.deb'
virt-customize -a ubuntu-24.04-server-cloudimg-amd64.img --run-command 'apt update'
virt-customize -a ubuntu-24.04-server-cloudimg-amd64.img --run-command 'apt install puppet-agent -y'
rm puppet.deb
qm destroy 10002
create_template 10002 "temp-ubuntu-24-04" "ubuntu-24.04-server-cloudimg-amd64.img"

#23.10 (Manic Minotaur)
#wget "https://cloud-images.ubuntu.com/releases/23.10/release/ubuntu-23.10-server-cloudimg-amd64.img"
#qm destroy 10003
#create_template 10003 "temp-ubuntu-23-10" "ubuntu-23.10-server-cloudimg-amd64.img"

## Fedora
#Image is compressed, so need to uncompress first
## Fedora 38

## Fedora 39
#wget "https://mirror.init7.net/fedora/fedora/linux/releases/39/Cloud/x86_64/images/Fedora-Cloud-Base-39-1.5.x86_64.raw.xz"
#xz -d -v Fedora-Cloud-Base-39-1.5.x86_64.raw.xz
#qm destroy 10004
#create_template 10005 "temp-fedora-39" "Fedora-Cloud-Base-39-1.5.x86_64.raw"
