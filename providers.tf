terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
      version = "0.60.0"
    }
  }
}

provider "proxmox" {
  endpoint = "https://p1proxmox3a.local.myops.ch:8006/"
  insecure = true
  ssh {
    agent    = true
    username = "terraform-prov"
  }
}

